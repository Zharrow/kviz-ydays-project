$(document).ready(function(){
    var controller = new ScrollMagic.Controller();
    var ourScene = new ScrollMagic.scene({
        triggerElement: '.lower_title_home',
        reverse: false
    })
        .setClassToggle('.lower_title_home', 'show')
        .addTo(controller);
});