PROBLÉMATIQUE ?
Comment donner envie aux gens d'apprendre
Comment réaliser un jeu éducatif qui soit à la portée de tous

POURQUOI ON EXISTE ?
Pour répondre à la problématique de la baisse de culture (souvent énoncé à la télévision par exemple)

ANNONCE DU PROJET
Développement d'un site web comportant des quizz à thème, apportant des certifications

DESCRIPTION
Le site comportera dans un premier temps un nombre de thème défini et à l'avenir un nb de thème illimité, ce qui
permettra à tous de trouver leurs thèmes de prédilection. Pour réussir à garder un certain entrain au jeu, le jeu 
comportera un mode solo & multijoueur, et compétitif ou non.

Il comportera une échelle de progression et également un classement sur ses résultats en solo.


SLOGAN
Apprendre différement qu'à l'école

ENJEUX
Améliorer les connaissances de la population

Idée pour le futur du projet :
- site multilingue 
- liaison avec des sites touristiques (monuments, musées, etc) avec une fonctionnalité de réalité augmenté
- un nb de thème illimité
- mini-jeux intéractif au cours du quizz, qui permet de dynamiser l'apprentissage (motion design + sound design)
- faire appel à des professionnels dans leurs domaines qui complèteront nos quizz pour approfondir le spectre
des compétences à acquérir



